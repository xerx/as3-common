﻿package  ik.code.observer
{

	public class Observable 
	{
		protected var observers:Vector.<IObserver>;
		
		public function Observable()
		{
			clearObservers();
		}
		public function register(observer:IObserver):void		
		{
			if(observers.indexOf(observer) > -1)
				return;
			observers.push(observer);
		}
		public function unregister(observer:IObserver):void		
		{
			var index:int = observers.indexOf(observer);
			if(index == -1)
				return;
			observers.removeAt(index);
		}
		public function notifyObservers(data:Object = null):void		
		{
			for(var i:int = observers.length - 1; i > -1; --i)
			{
				if(observers[i])
				{
					observers[i].notify(data);
				}
				else
				{
					observers.removeAt(i);
				}
			}
		}
		public function clearObservers():void
		{
			observers = new <IObserver>[];
		}
		public function destroy():void
		{
			observers = null;
		}
		

	}
	
}

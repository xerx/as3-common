﻿package ik.code.observer 
{

	public interface IObserver 
	{

		function notify(data:Object = null):void;

	}
	
}

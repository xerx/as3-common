﻿package ik.code.consts
{
	import ik.code.structs.Enum;
	
	public class ButtonState extends Enum
	{
		public static const UP:ButtonState = new ButtonState(1);
		public static const DOWN:ButtonState = new ButtonState(2);
		public static const DISABLED:ButtonState = new ButtonState(3);
		public static const FOCUSED:ButtonState = new ButtonState(4);
		
		public function ButtonState(explicitValue:Object = null)
		{
			super(explicitValue);
		}
	}
	
}

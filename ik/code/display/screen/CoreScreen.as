package ik.code.display.screen
{
	import ik.code.display.CoreMC;
	import ik.code.observer.IObserver;
	
	public class CoreScreen extends CoreMC implements IObserver
	{
		protected var data:IScreenData;
		public function CoreScreen()
		{
			super();
		}
		
		public function notify(data:Object=null):void
		{
			this.data = data as IScreenData; 
		}
		override public function destroy():void
		{
			data = null;
			super.destroy();
		}
	}
}
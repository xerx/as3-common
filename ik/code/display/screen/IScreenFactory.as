﻿package ik.code.display.screen 
{

	public interface IScreenFactory 
	{

		function getScreen(data:IScreenData):CoreScreen;

	}
	
}

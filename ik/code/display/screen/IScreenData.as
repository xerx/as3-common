﻿package ik.code.display.screen 
{

	
	public interface IScreenData 
	{
		function fromObject(object:Object):void;
		function toJSON():String;
		function destroy():void;
	}
	
}

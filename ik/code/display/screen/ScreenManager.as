﻿package ik.code.display.screen 
{
	import ik.code.observer.Observable;

	public class ScreenManager extends Observable
	{		
		protected var factory:IScreenFactory;
		protected var currentData:IScreenData;
		protected var currentScreen:CoreScreen;
		
		public function ScreenManager() 
		{
			
		}
		public function update(data:IScreenData):void
		{
			currentData = data;
			if(currentScreen)
				unregister(currentScreen);
			
			currentScreen = factory.getScreen(currentData);
			notifyObservers(currentData);
		}
		public function getCurrentScreen():CoreScreen
		{
			return currentScreen;
		}
		override public function destroy():void
		{
			factory = null;
			currentData.destroy();
			currentData = null;
			currentScreen.destroy();
			currentScreen = null;
			super.destroy();
		}
	}
	
}

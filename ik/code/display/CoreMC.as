﻿package ik.code.display
{
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.events.Event;
	
	import ik.code.events.EventObject;
	import ik.code.messages.IMessageEnabled;
	import ik.code.messages.MessageCenter;
	import ik.code.utils.ObjectUtil;
	import flash.utils.Dictionary;

	public class CoreMC extends MovieClip implements IMessageEnabled
	{
		private var eventObjects:Vector.<EventObject>;
		
		protected var messageCenter:MessageCenter;
		protected var _isActivated:Boolean;
		
		public function CoreMC() 
		{
			messageCenter = new MessageCenter(this);
			addEventListener(Event.ADDED_TO_STAGE, addedToStage);
		}
		protected function addedToStage(e:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, addedToStage);
		}
		public final function getChildren(...types):Dictionary
		{
			var childrenByType:Dictionary = new Dictionary();
			for(var k:int = 0; k < types.length; k++)
			{
				childrenByType[types[k]] = [];
			}
			var child:DisplayObject;
			for(var i:int = 0; i < numChildren; i++)
			{
				child = getChildAt(i);
				for(var j:int = 0; j < types.length; j++)
				{
					if(child is types[j])
					{
						childrenByType[types[j]].push(child);
					}
				}
			}
			return childrenByType;
		}
		public final function collectChildrenToVector(vector:*):*
		{
			const TYPE:Class = ObjectUtil.getTypeOfVector(vector);
			var child:DisplayObject;
			
			for(var i:int = 0; i < numChildren; i++)
			{
				child = getChildAt(i);
				if(child is TYPE)
					vector.push(child as TYPE);
			}
			return vector;
		}
		public final function getFirstChildOfType(type:*):*
		{
			var child:DisplayObject;
			for(var i:int = 0; i < numChildren; i++)
			{
				child = getChildAt(i);
				if(child is type)
					return child as type;
			}
			return null;
		}
		public final function getLastChildOfType(type:*):*
		{
			var child:DisplayObject;
			for(var i:int = numChildren - 1; i > -1; --i)
			{
				child = getChildAt(i);
				if(child is type)
					return child as type;
			}
			return null;
		}
		override public function addEventListener(type:String, 
												  listener:Function, 
												  useCapture:Boolean = false, 
												  priority:int = 0, 
												  useWeakReference:Boolean = false):void 
        {
			
			eventObjects ||= Vector.<EventObject>([]);
			const LEN:int = eventObjects.length;
			for(var i:int = 0; i < LEN; i++)
			{
				if(eventObjects[i].type == type)
					return;
			}
			
			eventObjects.push(new EventObject(type, listener));
			super.addEventListener(type, listener, false, 0, true);
		}
		override public function removeEventListener(type:String, 
												     listener:Function, 
												     useCapture:Boolean = false):void 
        {			
			eventObjects ||= Vector.<EventObject>([]);
			const LEN:int = eventObjects.length;
			var index:int = -1;
			
			for(var i:int = 0; i < LEN; i++)
			{
				if(eventObjects[i].type == type)
				{
					index = i;
					break;
				}
			}
			
			if(index == -1)return;
			
			super.removeEventListener(type, listener);
			eventObjects.splice(index, 1);
		}
		public final function removeListeners():void
		{
			if(!eventObjects)
				return;
			
			const LEN:int = eventObjects.length;
			
			for(var i:int = 0; i < LEN; i++)
				removeEventListener(eventObjects[0].type, eventObjects[0].listener);
		}
		public final function setSize(w:Number, h:Number):void
		{
			width = w;
			height = h;
		}
		public final function setPosition(X:Number, Y:Number):void
		{
			x = X;
			y = Y;
		}
		public final function removeFromParent():void
		{
			if(parent)
				parent.removeChild(this);
		}
		public function destroy():void
		{
			removeListeners();
			
			const LEN:int = numChildren;
			var child:DisplayObject;
			
			for(var i:int = 0; i < LEN; i++)
			{
				child = getChildAt(0);
				if(child)
				{
					removeChild(child);
					if(child is CoreMC)
						(child as CoreMC).destroy();
				}
			}
			eventObjects = null;
			if(messageCenter)
			{
				messageCenter.destroy();
				messageCenter = null;
			}
		}
		public final function get isActivated():Boolean
		{
			return _isActivated;
		}
		public function activate():void
		{
			_isActivated = true;
		}
		public function deactivate():void
		{
			_isActivated = false;
		}
		public function get msgc():MessageCenter
		{
			return messageCenter;
		}

	}
	
}

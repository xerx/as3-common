﻿package ik.code.display.data 
{
	import flash.display.Shape;
	import ik.code.display.CoreMC;
	import flash.display.GraphicsPathCommand;
	import flash.display.Graphics;
	import flash.utils.getTimer;
	import flash.utils.setInterval;
	import flash.utils.clearInterval;
	import flash.events.Event;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.geom.Rectangle;

	
	public class PercentageBar extends CoreMC
	{
		private var full:Shape;
		private var segment:Shape;
		private var field:TextField;
		
		private var percentage:Number;
		private var dims:Rectangle;
		private var duration:Number;
		private var color:uint;
		private var backgroundColor:uint;
		private var strokeColor:uint;
		private var strokeWeight:int;
		private var drawRate:Number;
		private var widthToDrawn:Number;
		
		public function PercentageBar(percentage:Number, 
									  dims:Rectangle, 
									  duration:Number, 
									  color:uint, 
									  backgroundColor:uint, 
									  strokeColor:uint, 
									  strokeWeight:int) 
		{
			this.percentage = percentage;
			this.dims = dims;
			this.duration = duration;
			this.color = color;
			this.backgroundColor = backgroundColor;
			this.strokeColor = strokeColor;
			this.strokeWeight = strokeWeight;
			
			init();
		}
		override protected function addedToStage(e:Event):void
		{
			super.addedToStage(e);
			
			x = dims.x;
			y = dims.y;
			widthToDrawn = dims.width * (percentage / 100);
			drawRate = widthToDrawn / (duration * stage.frameRate);
		}
		private final function init():void
		{
			full = new Shape();
			full.graphics.beginFill(backgroundColor);
			full.graphics.lineStyle(strokeWeight, strokeColor);
			full.graphics.drawRect(0, 0, dims.width, dims.height);
			full.graphics.endFill();
			addChildAt(full, 0);
			
			segment = new Shape();
			addChild(segment);
		}
		private final function draw(e:Event):void
		{
			if(segment.width + drawRate < widthToDrawn)
			{
				updateGraphics(segment.graphics, segment.width + drawRate, color);
			}
			else
			{
				updateGraphics(segment.graphics, widthToDrawn, color);
				stopAnimation();
			}
		}
		private final function updateGraphics(g:Graphics, p:Number, c:uint):void
		{
			g.clear();
			g.beginFill(c);
			g.lineStyle(strokeWeight, strokeColor);
			g.drawRect(0, 0, p, dims.height);
		}
		public final function updatePercentage(value:Number):void
		{
			percentage = value;
			if(!stage)return;
			widthToDrawn = dims.width * (percentage / 100);
			drawRate = widthToDrawn / (duration * stage.frameRate);
		}
		public final function clear():void
		{
			stopAnimation();
			segment.graphics.clear();
		}
		public final function startAnimation(instant:Boolean = false):void
		{		
			if(instant)
			{
				updateGraphics(segment.graphics, percentage, color);
				return;
			}
			addEventListener(Event.ENTER_FRAME, draw);
		}
		public final function stopAnimation():void
		{
			removeEventListener(Event.ENTER_FRAME, draw);
		}
		override public function destroy():void
		{
			full = null;
			segment = null;
		}
		
	}
	
}


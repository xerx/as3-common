﻿package ik.code.display.data 
{
	import flash.display.Shape;
	import ik.code.display.CoreMC;
	import flash.display.GraphicsPathCommand;
	import flash.display.Graphics;
	import flash.utils.getTimer;
	import flash.utils.setInterval;
	import flash.utils.clearInterval;
	import flash.events.Event;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;


	public class PercentagePie extends CoreMC
	{
		private var full:Shape;
		private var segment:Shape;
		private var hole:Shape;
		private var field:TextField;
		
		private var percentage:Number;
		private var size:int;
		private var duration:Number;
		private var color:uint;
		private var backgroundColor:uint;
		private var strokeColor:uint;
		private var donutRatio:Number;
		private var showPercentage:Boolean;
		private var degreesDrawn:Number;
		private var drawRate:Number;
		
		private var coords:Vector.<Number>;
		private var drawCommands:Vector.<int>;
		
		public function PercentagePie(percentage:Number, 
									  size:int, 
									  duration:Number, 
									  color:uint, 
									  backgroundColor:uint, 
									  strokeColor:uint, 
									  donutRatio:Number = 0,
									  showPercentage:Boolean = false) 
		{
			this.percentage = 360 * (percentage / 100);
			this.size = size;
			this.duration = duration;
			this.color = color;
			this.backgroundColor = backgroundColor;
			this.strokeColor = strokeColor;
			this.donutRatio = donutRatio;
			this.showPercentage = showPercentage;
			
			init();
		}
		override protected function addedToStage(e:Event):void
		{
			super.addedToStage(e);
			drawRate = 360/(duration * stage.frameRate);
		}
		private final function init():void
		{
			const TWO_PI:Number = Math.PI * 2;
			const RESOLUTION:int = 240;
			const STEP:Number = TWO_PI / RESOLUTION;
			
			var maxIndex:int = 0;

			coords = new <Number>[];
			drawCommands = new <int>[];

			for(var i:Number = 0; i < TWO_PI + STEP; i += STEP)
			{
			  coords.push(size * Math.cos(i));
			  coords.push(size * Math.sin(i));
			  drawCommands.push(GraphicsPathCommand.LINE_TO);
			}
			
			full = new Shape();
			full.rotation = -90;
			addChildAt(full, 0);
			
			segment = new Shape();
			segment.rotation = -90;
			addChild(segment);
			
			if(!donutRatio)return;
			
			donutRatio = donutRatio < .4 ? .4:donutRatio;
			
			hole = new Shape();
			hole.graphics.beginFill(strokeColor);
			hole.graphics.drawCircle(0, 0, size * donutRatio);
			hole.graphics.endFill();
			addChild(hole);
			
			if(!showPercentage)return;
			
			field = new TextField();
			field.selectable = false;			
			field.autoSize = TextFieldAutoSize.CENTER;
			updatePercentage((percentage * 100) / 360);
			field.visible = false;
			addChild(field);
		}
		private final function draw(e:Event):void
		{
			if(degreesDrawn >= 360)
			{
				updateGraphics(segment.graphics, percentage/360, color);
				
				if(percentage < 360)
					updateGraphics(full.graphics, 1, backgroundColor);
				stopAnimation();
			}
			else
			{
				if(degreesDrawn <= percentage)
				{
					updateGraphics(segment.graphics, degreesDrawn/360, color);
				}
				else
				{
					updateGraphics(segment.graphics, percentage/360, color);
					updateGraphics(full.graphics, degreesDrawn/360, backgroundColor);
				}
				degreesDrawn += drawRate;
			}
		}
		private final function updateGraphics(g:Graphics, p:Number, c:uint):void
		{
			var max:int = Math.ceil(p * drawCommands.length) * 2;
			
			g.clear();
			g.beginFill(c);
			
			if(p < 1)
				g.lineStyle(1, strokeColor);
			g.drawPath(drawCommands, coords.slice(0, max));
		}
		public final function updatePercentage(value:Number):void
		{
			percentage = 360 * (value / 100);
			
			if(!donutRatio || !showPercentage)return;
			
			field.text = value + "%";
			field.setTextFormat(new TextFormat("Arial", size * (donutRatio - .2), color));
			field.x = -field.width * .5
			field.y = -field.height * .5;
		}
		public final function clear():void
		{
			stopAnimation();
			full.graphics.clear();
			segment.graphics.clear();
			if(showPercentage)
				field.visible = false;
		}
		public final function startAnimation(instant:Boolean = false):void
		{
			degreesDrawn = 0;
			if(showPercentage)
				field.visible = true;
			
			if(instant)
			{
				updateGraphics(segment.graphics, percentage/360, color);
				updateGraphics(full.graphics, 1, backgroundColor);
				return;
			}
			addEventListener(Event.ENTER_FRAME, draw);
		}
		public final function stopAnimation():void
		{
			removeEventListener(Event.ENTER_FRAME, draw);
		}
		override public function destroy():void
		{
			full = null;
			segment = null;
			coords = null;
			drawCommands = null;
		}
		
	}
	
}


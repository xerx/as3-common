﻿package ik.code.display
{
	import flash.events.MouseEvent;
	import flash.events.Event;
	import ik.code.messages.Message;
	import flash.text.TextField;
	import ik.code.utils.StringUtil;
	import ik.code.consts.ButtonState;
	
	public class CoreButton extends CoreMC
	{
		public var isToggle:Boolean;		
		public var preventFrameChange:Boolean;		
		
		protected var _state:ButtonState;		
		protected var label:TextField;		
		
		public function CoreButton()
		{
			mouseChildren = false;
			label = getFirstChildOfType(TextField);
			
			setState(ButtonState.UP);
		}
		override public function activate():void
		{
			if(_isActivated)return;
			removeEventListener(MouseEvent.RELEASE_OUTSIDE, mouseReleased);
			removeEventListener(MouseEvent.MOUSE_UP, mouseReleased);
			addEventListener(MouseEvent.MOUSE_DOWN, mousePressed);
			_isActivated = buttonMode = true;
		}
		override public function deactivate():void
		{
			if(!_isActivated)return;
			removeEventListener(MouseEvent.MOUSE_DOWN, mousePressed);
			removeEventListener(MouseEvent.RELEASE_OUTSIDE, mouseReleased);
			removeEventListener(MouseEvent.MOUSE_UP, mouseReleased);
			_isActivated = buttonMode = false;
		}
		protected function mousePressed(e:MouseEvent):void
		{
			removeEventListener(MouseEvent.MOUSE_DOWN, mousePressed);
			addEventListener(MouseEvent.RELEASE_OUTSIDE, mouseReleased);
			addEventListener(MouseEvent.MOUSE_UP, mouseReleased);
			
			if(isToggle)
			{
				setState(_state == ButtonState.DOWN ? ButtonState.UP:ButtonState.DOWN)
			}
			else
			{
				setState(ButtonState.DOWN);
			}
		}
		protected function mouseReleased(e:MouseEvent):void
		{
			addEventListener(MouseEvent.MOUSE_DOWN, mousePressed);
			removeEventListener(MouseEvent.RELEASE_OUTSIDE, mouseReleased);
			removeEventListener(MouseEvent.MOUSE_UP, mouseReleased);
			
			if(!isToggle)
			{
				setState(ButtonState.UP);
				messageCenter.send(Message.CLICKED, false, this);
				//using SELECT to safely deprecate CLICKED
				if(messageCenter)
					messageCenter.send(Message.SELECT, false, this);
			}
			else
			{
				messageCenter.send(Message.PRESSED, false, this);
				//using SELECT to safely deprecate PRESSED
				if(messageCenter)
					messageCenter.send(Message.SELECT, false, this);
			}
		}
		public function getLabel():String
		{
			return label.text;
		}
		public function updateLabel(text:String):void
		{
			label.text = text;
			StringUtil.fitStrInTextField(label);
		}
		public final function get state():ButtonState
		{
			return _state;
		}
		public function setState(state:ButtonState):void
		{
			if(_state == state)return;
			
			_state = state;
			
			if(!preventFrameChange)
				gotoAndStop(_state);
			_state != ButtonState.DISABLED ? activate():deactivate();
		}
	}
}
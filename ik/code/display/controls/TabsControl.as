﻿package ik.code.display.controls 
{
	import ik.code.display.CoreButton;
	import ik.code.messages.Message;
	import ik.code.display.CoreMC;
	import flash.display.DisplayObject;
	import ik.code.enum.ButtonState;

	public class TabsControl extends CoreMC
	{
		protected var buttons:Vector.<CoreButton>;
		protected var _selectedIndex:int;
		
		public function TabsControl() 
		{
			buttons = new <CoreButton>[];
			
			var child:DisplayObject;
			for(var i:int = 0; i < numChildren; i++)
			{
				child = getChildAt(i);
				
				if(child is CoreButton)
				{
					buttons.push(child as CoreButton);
					buttons[buttons.length - 1].isToggle = true;
					buttons[buttons.length - 1].msgc.subscribe(Message.PRESSED, buttonSelected);
				}
			}
			_selectedIndex = -1;
		}
		public function set selectedIndex(value:int):void
		{
			_selectedIndex = value;
			
			for(var i:int = 0; i < buttons.length; i++)
				buttons[i].setState(i == _selectedIndex ? ButtonState.DISABLED:ButtonState.UP);
		}
		protected function buttonSelected(button:CoreButton):void
		{
			selectedIndex = buttons.indexOf(button);
			messageCenter.send(Message.UPDATED, false, _selectedIndex);
		}
		override public function activate():void
		{
			super.activate();
			for(var i:int = 0; i < buttons.length; i++)
				buttons[i].activate();
		}
		override public function deactivate():void
		{
			super.deactivate();
			for(var i:int = 0; i < buttons.length; i++)
				buttons[i].deactivate();
		}
		public function reset():void
		{
			if(_selectedIndex > -1)
				buttons[_selectedIndex].setState(ButtonState.UP);
			_selectedIndex = -1;
		}
		override public function destroy():void
		{
			buttons = null;
			super.destroy();
		}

	}
	
}

﻿package ik.code.structs 
{
	import flash.utils.getQualifiedClassName;
	
	public class Enum 
	{
		private static var counter:uint;
		
		public var value:Object;
		
		public function Enum(explicitValue:Object = null) 
		{
			this.value = explicitValue || String(++counter);
		}
		public function toString():String
		{
			return value.toString();
		}
	}
	
}

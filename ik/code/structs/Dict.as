﻿package  ik.code.structs
{
	import flash.utils.Dictionary;


	public dynamic class Dict extends Dictionary
	{
		public final function containsKey(key:String):Boolean
		{
			return key in this;
		}
		public final function containsValue(value:Object):Boolean
		{
			for each(var object:Object in this)
			{
				if(object === value)
					return true;
			}
			return false;
		}
		public final function toArray():Array
		{
			var array:Array = [];
			for each(var object:Object in this)
				array.push(object);
			return array;
		}
		public final function clear():void
		{
			for(var key:String in this)
				delete this[key];
		}
		public final function get length():uint
		{
			var n:uint;
			for(var key:String in this)
				n++;
			return n;
		}
		public final function toString():String
		{
			var str:String = "{";
			for(var key:String in this)
				str += key + ":" + this[key] + ", ";
			str = str.length > 1 ? str.substr(0, str.length - 2) + "}":"{}";
			return str;
		}
	}
	
}

﻿package ik.code.structs 
{

	public class TreeNode 
	{
		public var value:Object;
		public var parent:TreeNode;
		
		private var _children:Vector.<TreeNode>;
		
		public function TreeNode(value:Object = null, ...children) 
		{
			this.value = value;
			_children = new <TreeNode>[];
			
			if(_children.length)
				addChildren(children);
		}
		public function addChildren(newChildren:Array):void
		{
			for(var i:int = 0; i < newChildren.length; i++)
			{
				_children.push(newChildren[i] as TreeNode);
				newChildren[i].parent = this;
			}
		}
		public function removeChild(index:int):TreeNode
		{
			if(index >= _children.length)
				return null;
			return _children.removeAt(index) as TreeNode;
		}
		public function getDescendant(...indeces):TreeNode
		{
			if(isLeaf || indeces.length == 0)
				return this;
			if(indeces.length == 1)
			{
				if(indeces[0] is Array)
				{
					var descendantsArray:Array = indeces[0];
					if(descendantsArray.length == 1)
						return _children[descendantsArray[0]];
					return _children[descendantsArray.shift()].getDescendant(descendantsArray);
				}
				else
				{
					return _children[indeces[0]];
				}
			}
			return _children[indeces.shift()].getDescendant.apply(null, indeces);
		}
		public function getDescendantByField(fieldName:String, fieldValue:String):TreeNode
		{
			if(this[fieldName] == fieldValue)
				return this;
			
			var node:TreeNode;
			for (var i:int = 0; i < _children.length; i++) 
			{
				node = _children[i].getDescendantByField(fieldName, fieldValue);
				if(node)return node;
			}
			return null;
		}
		public function getSiblings():Vector.<TreeNode>
		{
			if(isRoot)return null;
			return parent.children;
		}
		public function get children():Vector.<TreeNode>
		{
			return _children;
		}
		public function get parentPathToSelf():int
		{
			if(isRoot)return -1;
			return parent.children.indexOf(this);
		}
		public function get rootPathToSelf():Array
		{
			if(isRoot)return [];
			var path:Array = [parentPathToSelf];
			var nodeParent:TreeNode = parent;
			
			while(nodeParent && !nodeParent.isRoot)
			{
				path.push(nodeParent.parentPathToSelf);
				nodeParent = nodeParent.parent;
			}
			return path.reverse();
		}
		public function get hasSiblings():Boolean
		{
			return getSiblings().length > 0;
		}
		public function get isLeaf():Boolean
		{
			return _children.length == 0;
		}
		public function get isRoot():Boolean
		{
			return parent == null;
		}
		public function destroy():void
		{
			value = null;
			parent = null;
			
			for(var i:int = 0; i < _children.length; i++)
				_children[i].destroy();
			_children = null;
		}
	}
	
}

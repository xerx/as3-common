﻿package ik.code.events 
{
	
	public class EventObject 
	{
		private var _type:String;
		private var _listener:Function;
		
		public function EventObject(type:String, listener:Function):void 
		{
			_type = type;
			_listener = listener;
		}
		public function get listener():Function
		{
			return _listener;
		}
		public function get type():String
		{
			return _type;
		}

	}
	
}

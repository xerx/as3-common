﻿package ik.code.utils 
{
	import flash.text.TextField;
	import flash.text.TextFormat;


	public class StringUtil 
	{
		public static function clearExtraSpaces(str:String):String
		{
			return str.replace(/(\t|\n|\s{1,})/g, " ");
		}
		public static function trim(str:String):String
		{
			return str.replace(/^\s+|\s+$/g, "");
		}
		public static function fitStrInTextField(field:TextField):void
		{
			var format:TextFormat = field.getTextFormat();
			
			while(field.textWidth > field.width * .9)
			{
				format.size = int(format.size) - 1;
				field.setTextFormat(format);
				field.y += .5;
			}
		}
		public static function indexOf(pattern:String, str:String, occurenceIndex:int = 1):int
		{
			var i:int = -1;
			while(occurenceIndex--)
				i = str.indexOf(pattern, i + 1);
			
			return i;
		}

	}
	
}

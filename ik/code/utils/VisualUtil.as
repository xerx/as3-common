﻿package ik.code.utils
{
	import flash.display.Shape;
	import flash.display.Graphics;
	import flash.display.LineScaleMode;
	import flash.filters.DropShadowFilter;
	import flash.filters.GlowFilter;
	import flash.filters.ColorMatrixFilter;
	import fl.motion.AdjustColor;
	import flash.filters.BevelFilter;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.geom.ColorTransform;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.geom.Rectangle;
	import flash.display.StageQuality;
	import flash.display.PixelSnapping;
	import flash.geom.Matrix;

	public class VisualUtil
	{
		
		public static const DSF:DropShadowFilter = new DropShadowFilter();
		public static const GF:GlowFilter = new GlowFilter();
		public static const BLUE_GF:GlowFilter = new GlowFilter(0x0066FF, 1, 6, 6);
		public static const RED_GF:GlowFilter = new GlowFilter(0xCC0000, 1, 6, 6);
		public static const GREEN_GF:GlowFilter = new GlowFilter(0x009900, 1, 6, 6);
		public static const BF:BevelFilter = new BevelFilter();
		
		private static var grayscaleColorFilter:ColorMatrixFilter;
		private static var colorTransform:ColorTransform = new ColorTransform();
		
		public static function getRect(width:Number, height:Number, color:Object, alpha:Number = 1, 
									   strokeWeight:Object = null, strokeColor:Object = null):Shape
		{
			const shape:Shape = new Shape();
			
			if(strokeWeight)
				shape.graphics.lineStyle(int(strokeWeight), uint(strokeColor));
			if(color != null)
				shape.graphics.beginFill(uint(color), alpha);
			shape.graphics.drawRect(0, 0, width, height);
			shape.graphics.endFill();

			return shape;
		}
		public static function applyMaskToObject(object:DisplayObjectContainer, width:Number, height:Number):void
		{
			var mask:Shape = VisualUtil.getRect(width, height, 0);
			object.addChild(mask);
			object.mask = mask;
		}
		public static function get grayscaleFilter():ColorMatrixFilter
		{
			if(grayscaleColorFilter)
				return grayscaleColorFilter;
			
			var color:AdjustColor = new AdjustColor();
			color.brightness = 0;
			color.contrast = 0;
			color.hue = 0;
			color.saturation = -100;
			
			grayscaleColorFilter = new ColorMatrixFilter(color.CalculateFinalFlatArray());
			
			return grayscaleColorFilter;
		}
		public static function tint(object:DisplayObject, color:uint):void
		{
			colorTransform.color = color;
			colorTransform.alphaMultiplier = object.alpha;
			object.transform.colorTransform = colorTransform;
		}
		public static function getBitmapFromDrawable(drawable:DisplayObject, fillColor:uint = 0, smooth:Boolean = true, matrix:Matrix = null):Bitmap
		{
			var tempMatrix:Matrix = drawable.transform.matrix;
			var ct:ColorTransform;
			
			if(matrix)
				drawable.transform.matrix = matrix;
			
			if(fillColor)
			{
				ct = new ColorTransform();
				ct.color = fillColor;
			}
			
			var data:BitmapData = new BitmapData(drawable.width, drawable.height, true, 0xFFFFFF);
			data.drawWithQuality(drawable, matrix, ct, null, null, smooth, StageQuality.BEST);
			
			var bitmap:Bitmap = new Bitmap(data, PixelSnapping.AUTO, smooth);
			
			if(matrix)
			{
				drawable.transform.matrix = tempMatrix;
				bitmap.width = drawable.width;
				bitmap.height = drawable.height;
			}
			
			return bitmap; 
		}
	}

}
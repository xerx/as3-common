﻿package ik.code.utils 
{
	import flash.utils.ByteArray;
	import flash.display.MovieClip;
	import flash.utils.getQualifiedClassName;
	import flash.utils.getDefinitionByName;
	import flash.utils.describeType;
	import flash.utils.Dictionary;

	public class ObjectUtil 
	{
		public static function cloneObject(object:*):* 
		{
            var byteArray:ByteArray = new ByteArray();
			byteArray.writeObject(object);
			byteArray.position = 0;
			
			return byteArray.readObject();            
		}
		public static function cloneMC(mc:MovieClip):* 
		{
			const MCClass:Class = mc["constructor"];
			var clone:MovieClip = new MCClass();
			
			return clone;            
		}
		public static function getTypeOfVector(v:*):Class
		{
			var type:String = getQualifiedClassName(v);
			
			type = type.split("<")[1];
			type = type.substr(0, type.length - 1);
			
			return getDefinitionByName(type) as Class;
		}
	}
	
}

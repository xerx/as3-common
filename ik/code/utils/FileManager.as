﻿package ik.code.utils
{
	import flash.filesystem.File;
	import flash.filesystem.FileStream;
	import flash.filesystem.FileMode;
	import flash.events.Event;
	import flash.utils.ByteArray;
	import flash.utils.CompressionAlgorithm;

	public class FileManager
	{
		private var file:File;
		private var overrideSecurityFile:File;
		private var stream:FileStream;
		
		public var rootPath:String;
		
		public function FileManager(rootPath:String)
		{
			this.rootPath = rootPath;
			setupPath();
			stream = new FileStream();
		}
		private function setupPath():void
		{
			file = new File(rootPath);
			
			if(file.exists)
				return;
			
			var pathParts:Array = rootPath.split("/");
			var tempPath:String = "";
			
			for(var i:int = 0; i < pathParts.length; i++)
			{
				tempPath += pathParts[i] + "/"
				file = new File(tempPath);
				
				if(file.exists)
					continue;
				
				file.createDirectory();
			}
		}
		public function readTextFile(relativePath:String):String
		{
			file = new File(rootPath + relativePath);
			
			if(!file.exists)
				return null;
			
			stream.open(file, FileMode.READ);
			
			var text:String;
			text = stream.readUTFBytes(stream.bytesAvailable);
			stream.close();
			
			return text;
		}
		public function writeTextFile(relativePath:String, str:String):void
		{
			overrideSecurityFile = new File(rootPath + relativePath);
			file = new File(overrideSecurityFile.nativePath);
			
			stream.open(file, FileMode.WRITE);
			stream.writeUTFBytes(str);
			stream.close();
		}
		public function readObjectBytes(relativePath:String):Object
		{			
			file = new File(rootPath + relativePath);

			if(!file.exists)
				return null;
			
			var ba:ByteArray = new ByteArray();
			
			stream.open(file, FileMode.READ);
			stream.readBytes(ba);
			stream.close();
			
			ba.position = 0;
			ba.uncompress(CompressionAlgorithm.DEFLATE);
			
			return ba.readObject();
		}
		public function writeObjectBytes(relativePath:String, object:Object):void
		{
			overrideSecurityFile = new File(rootPath + relativePath);
			file = new File(overrideSecurityFile.nativePath);
			
			var ba:ByteArray = new ByteArray();
			ba.writeObject(object);
			
			ba.position = 0;
			ba.compress(CompressionAlgorithm.DEFLATE);
			
			stream.open(file, FileMode.WRITE);
			stream.writeBytes(ba, 0, ba.length);
			stream.close();
		}
		public function readXMLFile(relativePath:String):XML
		{			
			return XML(readTextFile(relativePath));
		}
		public function writeXMLFile(relativePath:String, str:String):void
		{			
			writeTextFile(relativePath, XML(str).toXMLString());
		}
		public function deleteFile(relativePath:String):void
		{	
			overrideSecurityFile = new File(rootPath + relativePath);
			file = new File(overrideSecurityFile.nativePath);
			
			if(!file.exists)
				return;

			if(file.isDirectory)
				file.deleteDirectory(true);
			else
				file.deleteFile();
		}
		public function getFileNamesOfType(type:String, relativePath:String = ""):Vector.<String>
		{	
			var fileNames:Vector.<String> = new <String>[];
			
			file = new File(rootPath + relativePath);
			var files:Array = file.getDirectoryListing();
			
			for(var i:int = 0; i < files.length; i++)
			{
				if(files[i].name.search("." + type) == -1)
					continue;
				
				fileNames.push(files[i].name.replace("." + type, ""));
			}
			return fileNames;
		}
		public function destroy():void
		{
			file = null;
			overrideSecurityFile = null;
			stream = null;
		}
		

	}
	
}

﻿package ik.code.utils
{
	import flash.geom.Point;
	import flash.utils.getTimer;
	
	public class MathUtil
	{
		public static function abs(x:Number):Number
		{
			return x > 0 ? x:-x;
		}
		public static function toInt(x:Number):int
		{
			return x >> 0;
		}
		public static function isEven(x:int):Boolean
		{
			return (x & 1) == 0;
		}
		public static function pseudoRandom():Number
		{
			var r:Number = Math.sin(getTimer());
			return r > 0 ? r:-r;
		}
		public static function randomSign():int
		{
			return Math.random() < .5 ? -1:1;
		}
		public static function pointsCenter(p1:Point, p2:Point):Point
		{
			var centerX:Number = MathUtil.abs(p1.x - p2.x) >> 1;
			var centerY:Number = MathUtil.abs(p1.y - p2.y) >> 1;
			
			centerX += (p1.x < p2.x ? p1.x:p2.x);
			centerY += (p1.y < p2.y ? p1.y:p2.y);
			
			return new Point(centerX, centerY);
		}
		public static function calculateDistance(fromPoint:Point, toPoint:Point):Number
		{
			return Math.sqrt((toPoint.x - fromPoint.x) * (toPoint.x - fromPoint.x) + 
							 (toPoint.y - fromPoint.y) * (toPoint.y - fromPoint.y));
		}
		
		public static function roundWithDecimals(value:Number, numDecimals:int):Number
		{
			var multiplier:int = Math.pow(10, numDecimals);
			return Math.round(value * multiplier) / multiplier;
		}
		public static function ceilWithDecimals(value:Number, numDecimals:int):Number
		{
			var multiplier:int = Math.pow(10, numDecimals);
			return Math.ceil(value * multiplier) / multiplier;
		}
		public static function degreesToRadians(degrees:Number):Number
		{
			return degrees * (Math.PI / 180);
		}
		public static function radiansToDegrees(radians:Number):Number
		{
			return radians * (180 / Math.PI);
		}
	}

}